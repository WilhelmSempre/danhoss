<?php
    $title = 'Danhoss Group - Partybox, Crazyshop';

    $page = $_SERVER['PHP_SELF'];
    $page = basename($page);
    $page = str_replace('.php', '', $page);
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title><?php echo sprintf('%s - %s', $title, $pageTitle); ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700,900&amp;subset=latin-ext">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700">
    <link rel="stylesheet" href="assets/css/dist/style.min.css">
</head>
<body>
<header class="site-header container">

    <div class="logo-wrapper">
        <button class="hamburger hamburger--collapse" type="button">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
        </button>
        <a href="index.php"><img class="site-logo" src="assets/img/logo.svg" alt="Danhoss Group - positive emotions!"></a>
    </div>

    <nav class="site-navigation">
        <ul class="site-navigation-list">
            <li class="site-navigation-list-item">
                <a href="carrier" class="site-navigation-list-link <?php echo $page === 'carrier' ? 'active' : ''; ?>">Kariera</a>
            </li>
            <li class="site-navigation-list-item">
                <a href="where" class="site-navigation-list-link <?php echo $page === 'where' ? 'active' : ''; ?>">Gdzie</a>
            </li>
            <li class="site-navigation-list-item">
                <a href="what" class="site-navigation-list-link <?php echo $page === 'what' ? 'active' : ''; ?>">Co</a>
            </li>
            <li class="site-navigation-list-item">
                <a href="why" class="site-navigation-list-link <?php echo $page === 'why' ? 'active' : ''; ?>">Dlaczego</a>
            </li>
            <li class="site-navigation-list-item">
                <a href="how" class="site-navigation-list-link <?php echo $page === 'how' ? 'active' : ''; ?>">Jak</a>
            </li>
            <li class="site-navigation-list-item">
                <a href="index.php" class="site-navigation-list-link <?php echo $page === 'index' ? 'active' : ''; ?>">Home</a>
            </li>
        </ul>
    </nav>
</header>