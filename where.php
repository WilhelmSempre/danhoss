<?php
    $pageTitle = 'Gdzie';
?>

<?php require_once 'head.php'; ?>
<main class="wrapper container">
    <section class="where-section">
        <div class="where-section-image-wraper wow zoomIn">
            <img class="where-image" src="assets/img/chelmno.jpg" alt="Chełmno - miasto zakochanych">
            <p class="where-description"><b>Tutaj powstają najlepsze pomysły <br>na okazywanie uczuć.</b></p>
        </div>

        <aside class="where-locations odd">
            <figure class="where-location where-location-image">
                <div class="where-location-video-container">
                    <video autoplay loop>
                        <source src="assets/img/biskupia.mp4" type="video/mp4">
                        <!--                    <source src="movie.ogg" type="video/ogg">-->
                        Your browser does not support the video tag.
                    </video>
                </div>
            </figure>
            <div class="where-location where-location-description">
                <p class="where-location-description-text"><span>Dawniej fabryka klamek, dziś</span></p>
                <p class="where-location-description-text"><span>produkujemy tu inspiracje na imprezy</span></p>
                <p class="where-location-description-text"><span>które pozwalają otwierać się na ludzi</span></p>
            </div>
        </aside>
        <aside class="where-locations even">
            <figure class="where-location where-location-image">
                <img class="where-location-image-content" src="assets/img/lunawska.jpg" alt="Łunawska">
            </figure>
            <div class="where-location where-location-description">
                <p class="where-location-description-text"><span>Nasza codzienna praca</span></p>
                <p class="where-location-description-text"><span>to dostarczanie pozytywnych emocji</span></p>
                <p class="where-location-description-text"><span>milionom ludzi na miliony okazji</span></p>
            </div>
        </aside>
    </section>
</main>
<?php require_once 'footer.php' ?>
