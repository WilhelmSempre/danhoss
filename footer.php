        <footer class="site-footer">
            <address class="footer-wrapper company-address">
                DANHOSS Sp. z o.o. Sp. Komandytowa<br>
                ul. Biskupia 14<br>
                86-200 Chełmno<br>
                NIP: 875-155-67-88
            </address>
            <aside class="footer-wrapper company-contact">
                Recepcja<br>
                tel. +48 566 790 812<br>
                wew. 1<br>
                <a class="company-contact-link" href="mailto:recepcja@danhoss.com">recepcja@danhoss.com</a><br>
                <a class="company-contact-link policy highlight" href="policies">Polityka prywatności</a>
            </aside>
            <aside class="footer-wrapper footer-logo-wrapper">
                <img class="footer-logo" src="assets/img/logo-dark.svg" alt="Danhoss Group - positive emotions!">
            </aside>
        </footer>
        <script src="assets/js/dist/main.min.js"></script>
    </body>
</html>