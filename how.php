<?php
    $pageTitle = 'Jak';
?>

<?php require_once 'head.php'; ?>
<main class="wrapper container">
    <section class="how-section">
        <figure class="how-slide odd">
            <img class="how-slide-image" src="assets/img/responsibility.jpg" alt="Responsibility">
            <figcaption class="how-slide-image-caption">
                <p class="how-slide-image-caption-text header"><span class="highlight">Odpowiedzialnie</span></p>
                <p class="how-slide-image-caption-text"><span>Cały zespół dokłada starań, aby nasze produkty były najwyższej jakości</span></p>
                <p class="how-slide-image-caption-text"><span>Nasi klienci mogą liczyć na staranną obsługę z zachowaniem procedur</span></p>
                <p class="how-slide-image-caption-text"><span>bezpieczeństwa podczas zakupów on-line.</span></p>
            </figcaption>
        </figure>
        <figure class="how-slide even">
            <img class="how-slide-image" src="assets/img/ideas.jpg" alt="Ideas">
            <figcaption class="how-slide-image-caption">
                <p class="how-slide-image-caption-text header"><span class="highlight">Z bagażem pozytywnych emocji</span></p>
                <p class="how-slide-image-caption-text"><span>Chcąc dawać ludziom pozytywne emocje, najpierw trzeba mieć je w sobie</span></p>
                <p class="how-slide-image-caption-text"><span>Radość z tworzenia i wiara w dobry design to jest to, co nas nakręca.</span></p>
            </figcaption>
        </figure>
        <figure class="how-slide odd">
            <img class="how-slide-image" src="assets/img/experiences.jpg" alt="Experiences">
            <figcaption class="how-slide-image-caption">
                <p class="how-slide-image-caption-text header"><span class="highlight">Zawsze do przodu</span></p>
                <p class="how-slide-image-caption-text"><span>Uczymy się każdego dnia. Zawsze szukamy rozwiązań przynoszących</span></p>
                <p class="how-slide-image-caption-text"><span>progress. Z uwagą śledzimy nowoczesne trendy, aby jak najszybciej je wdrożyć.</span></p>
            </figcaption>
        </figure>
    </section>
</main>
<?php require_once 'footer.php' ?>
