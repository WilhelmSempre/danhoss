<?php
    $pageTitle = 'Co';
?>

<?php require_once 'head.php'; ?>
<main class="wrapper">
    <section class="what-slide-section">
        <figure class="what-slide odd">
            <figcaption class="what-slide-image-caption">
                <p class="what-slide-image-caption-text"><span>Tworzymy produkty i usługi,</span></p>
                <p class="what-slide-image-caption-text"><span>które dają radość, pozytywną energię,</span></p>
                <p class="what-slide-image-caption-text"><span>wywołują uśmiech</span></p>
            </figcaption>
            <img class="what-slide-image" src="assets/img/what.gif" alt="What">
        </figure>
    </section>
    <section class="what-section">
        <p class="what-info">Wierzymy w internet, działamy w obszarze e-commerce od 2006 roku.</p>

        <section class="what-shops container">
            <aside class="what-shop">
                <div class="what-shop-logo">
                    <a class="what-shop-link" href=""><img class="what-shop-image" src="assets/img/partybox.svg" alt="Partybox"></a>
                </div>
                <p class="what-shop-description">
                    Partybox.pl to sklep internetowy z akcesoriami na imprezy. <span class="more">Znajdziesz w nim m. in. stroje karnawałowe, dekoracje, opakowania na prezenty, balony, zaproszenia, peruki, piniaty i wiele innych gadżetów. Z nami urządzisz każde przyjęcie – roczek, urodziny, wesele, halloween, chrzest i inne. Dzięki Partybox.pl zaplanujesz zabawę od A do Z bez wychodzenia z domu: nakryjesz do stołu, udekorujesz mieszkanie, a także wyślesz wyjątkowe zaproszenia.</span>
                </p>
            </aside>
            <aside class="what-shop">
                <div class="what-shop-logo">
                    <a class="what-shop-link" href=""><img class="what-shop-image" src="assets/img/crazyshop.gif" alt="Crazyshop"></a>
                </div>
                <p class="what-shop-description">
                    Crazyshop.pl to kopalnia niebanalanych prezentów na różne okazje. <span class="more">W swojej ofercie posiadamy wyjątkowe upominki, które przypadną do gustu zarówno kobietom jak i mężczyznom. Dzięki nam znajdziesz prezent idealny dla męża, przyjaciółki czy babci. Możesz umieścić na nim co tylko chcesz: imię, datę ślubu, zabawny tekst. Crazyshop.pl oferuje także prezenty dla najmłodszych, np. poduszki, pojemniki na drugie śniadanie, skarbonki i wiele innych drobiazgów.</span>
                </p>
            </aside>
        </section>
    </section>
</main>
<?php require_once 'footer.php' ?>
