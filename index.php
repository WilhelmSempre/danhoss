<?php
    $pageTitle = 'Home';
?>

<?php require_once 'head.php'; ?>
<main class="wrapper container">
    <section class="main-section">
        <figure class="main-slide odd">
            <img class="main-slide-image" src="assets/img/emotions.jpg" alt="Emotions">
            <figcaption class="main-slide-image-caption">
                <p class="main-slide-image-caption-text"><span>Tworzymy i przekazujemy</span></p>
                <p class="main-slide-image-caption-text"><span><b class="highlight">pozytywne emocje</b></span></p>
                <p class="main-slide-image-caption-text"><span>to do nas wraca i to nas nakręca</span></p>
            </figcaption>
        </figure>

        <figure class="main-slide even">
            <img class="main-slide-image" src="assets/img/wow.jpg" alt="WOW!">
            <figcaption class="main-slide-image-caption">
                <p class="main-slide-image-caption-text"><span>Jesteśmy kreatywnym</span></p>
                <p class="main-slide-image-caption-text"><span>zgranym zespołem</span></p>
                <p class="main-slide-image-caption-text"><span>projektującym na co dzień</span></p>
                <p class="main-slide-image-caption-text"><span><b class="highlight">efekt WOW</b></span></p>
            </figcaption>
        </figure>

        <figure class="main-slide odd">
            <img class="main-slide-image" src="assets/img/products.jpg" alt="Products">
            <figcaption class="main-slide-image-caption">
                <p class="main-slide-image-caption-text"><span>Lubimy dobrą jakość</span></p>
                <p class="main-slide-image-caption-text"><span>dajemy Wam <b class="highlight">unikalne pomysły</b></p>
                <p class="main-slide-image-caption-text"><span>w niezwykłych produktach</span></p>
            </figcaption>
        </figure>

        <section class="about">
            <p class="about-header wow bounceInUp" data-wow-duration="1s">positive e-motions</p>
            <p class="about-description wow bounceInUp" data-wow-duration="2s">
                Działamy w przestrzeni cyfrowej, ale dajemy
                ludziom prawdziwe emocje.<br>
                DANHOSS GROUP posiada 2 sklepy:
                CrazyShop.pl i PartyBox.pl.<br>
                Łączy je pozytywny przekaz i dobra
                jakość produktów.
            </p>

            <ul class="about-shops wow bounceInUp" data-wow-duration="2s">
                <li class="about-shops-item"><a class="about-shops-link" href=""><img class="about-shops-image" src="assets/img/partybox.svg" alt="Partybox"></a></li>
                <li class="about-shops-item"><a class="about-shops-link" href=""><img class="about-shops-image" src="assets/img/crazyshop.gif" alt="Crazyshop"></a></li>
            </ul>
        </section>
    </section>
</main>
<?php require_once 'footer.php' ?>
