<?php
    $pageTitle = 'Polityka prywatnosci';
?>

<?php require_once 'head.php'; ?>
<main class="wrapper container policies">
    <section class="policies-section">

        <h3><strong>Polityka Prywatności</strong></h3>

        <p class="policies-section-text">Dowiedz się na jakich zasadach chronimy Twoje dane.</p>

        <p class="policies-section-text">Z poniższych punktów dotyczących ochrony danych dowiesz się, które z Twoich danych przetwarzane są przez nas, gdy odwiedzasz naszą stronę, zakładkę rekrutacyjną oraz, gdy bierzesz udział w procesie rekrutacyjnym.</p>

        <p class="policies-section-paragraph">1. Jakie dane przetwarzamy, gdy odwiedzasz tą stronę?</p>
        <p class="policies-section-text">Gdy odwiedzasz tą stronę, utworzone zostają pliki protokołu zawierające następujące informacje:</p>
        <ul class="policies-section-list">
            <li>Twój adres IP</li>
            <li>Ścieżka strony</li>
            <li>Opis typu używanej aplikacji klienckiej (np. przeglądarka internetowa)</li>
            <li>Strona internetowa, przez którą trafili Państwo na tę stronę (referer)</li>
            <li>Data i czas zapytania</li>
        </ul>

        <p class="policies-section-text">Pliki protokołu przechowywane są przez 14 dni, umożliwia to pozyskanie informacji o potencjalnych sprawcach w razie ataków hakerskich.</p>
        <p class="policies-section-paragraph">2. Jakie dane przetwarzamy, gdy odwiedzacie zakładkę rekrutacyjną?</p>
        <p class="policies-section-text">Pliki cookie tworzone są z przyczyn technicznych w momencie uruchomienia zakładki "Ty". Pliki cookie to niewielkie informacje w formie ciągu tekstowego, które wysyłane są przez nasz serwer WWW na Twoje urządzenie końcowe, następnie zostają one zapisane na zainstalowanej na tym urządzeniu przeglądarce internetowej. Pozwalają one na przechowywanie informacji przez określony czas oraz na identyfikowanie urządzenia końcowego użytkownika. Pliki cookie nie wyrządzą jakichkolwiek szkód na Twoim urządzeniu końcowym, nie zawierają one także wirusów, trojanów lub innego złośliwego oprogramowania. Do obsługi Twoich odwiedzin na stronie używane są wyłącznie tzw. cookies sesyjne, które usuwane są w sposób automatyczny po zamknięciu przeglądarki. Oznacza to, że jakiekolwiek dane nie zostaną zapisane na Twoim urządzeniu końcowym na stałe. Możesz zmienić ustawienia swojej przeglądarki w taki sposób, że będziesz otrzymywać informacje o umieszczeniu plików cookie w przeglądarce; korzystanie z plików cookie stanie się wtedy bardziej przejrzyste.</p>
        <p class="policies-section-paragraph">3. Jakie dane przetwarzamy w ramach Twojego procesu rekrutacyjnego?</p>
        <p class="policies-section-text">Twoje dokumenty rekrutacyjne są przetwarzane wyłącznie w celach wyboru kandydatów i ich zatrudnienia. Dokumenty przetwarzane są na podstawie art. 22 Kodeksu Pracy.</p>
        <p class="policies-section-text">Gdy tylko Twoje dokumenty rekrutacyjne zostaną zapisane na naszym portalu rekrutacyjnym, otrzymasz automatyczne potwierdzenie na podany adres e-mail. Następnie zweryfikujemy dokumenty aplikacyjne i skontaktujemy się z Tobą.</p>
        <p class="policies-section-paragraph">4. Jak długo przechowujemy Twoje dane?</p>
        <p class="policies-section-text">Jeżeli proces rekrutacyjny zakończy się powodzeniem, Twoje dane zostaną włączone do akt osobowych. Jeżeli proces rekrutacyjny nie zakończy się powodzeniem, Twoje dane przechowywane będą jeszcze przez 12 miesięcy od wpłynięcia powiadomienia o negatywnym wyniku aplikacji. Po tym czasie Twoje dane zostaną usunięte. Jeżeli zrezygnujesz z procesu rekrutacyjnego, Twoje dane zostaną skasowane w ciągu 24 godzin od otrzymania oświadczenia o rezygnacji z udziału w rekrutacji. Zapisane w kopiach roboczych dokumenty aplikacyjne, które nie zostały wysłane, zostaną usunięte po 7 dniach.</p>
        <p class="policies-section-paragraph">5. Twoje prawa w związku z przetwarzaniem danych osobowych.</p>
        <p class="policies-section-text">Przysługuje Tobie prawo do uzyskania informacji na temat danych osobowych, żądania dostępu do ich treści, sprostowania, usunięcia lub ograniczenia przetwarzania, prawo do wniesienia sprzeciwu wobec przetwarzania, a także prawo do przenoszenia danych. Ponadto masz prawo do wniesienia skargi do Urzędu Ochrony Danych Osobowych. Podanie danych osobowych jest dobrowolne. W przypadku, jeżeli podstawą przetwarzania jest zgoda, możesz w dowolnym momencie ją cofnąć. Konsekwencją niepodania danych lub cofnięcia zgody jest brak możliwości wzięcia udziału w procesie rekrutacji.</p>
        <p class="policies-section-paragraph">6. Do kogo możesz się zwrócić w razie pytań odnośnie procesu rekrutacyjnego lub skorzystania z przysługujących Tobie praw?</p>
        <p class="policies-section-text">W razie pytań odnośnie procesu rekrutacyjnego lub skorzystania z przysługujących Tobie praw związanych z przetwarzaniem danych osobowych możesz zwrócić do działu HR (<a href="mailto:mdembek@danhoss.com">mdembek@danhoss.com</a>).</p>

    </section>
</main>
<?php require_once 'footer.php' ?>
