<?php
    $pageTitle = 'Kariera';
?>

<?php require_once 'head.php'; ?>
<main class="wrapper container">
    <section class="carrier-section">
        <figure class="carrier-slide odd">
            <figcaption class="carrier-slide-image-caption">
                <p class="carrier-slide-image-caption-text"><span>Jeśli chcesz zostać <b class="highlight">jednym z nas</b></span></p>
                <p class="carrier-slide-image-caption-text"><span>pomyśl, czy zaakcetujesz pewne fakty:</span></p>
            </figcaption>
            <img class="carrier-slide-image" src="assets/img/ty.jpg" alt="Ty">
        </figure>

        <aside class="carrier-slide-second even">
            <figure class="carrier-slide-second-content carrier-slide-second-image">
                <img class="carrier-slide-second-image-content" src="assets/img/SM.jpg" alt="Supermoce">

                <figcaption class="carrier-slide-second-image-caption">
                    <p class="carrier-slide-second-image-caption-text"><span>... a jakie są Twoje <b class="highlight">supermoce</b>?</span></p>
                </figcaption>
            </figure>
            <div class="carrier-slide-second-content carrier-slide-second-description">
                <p class="carrier-slide-second-description-text typed"></p>
            </div>
        </aside>
        <aside class="carrier-slide-second odd">
            <figure class="carrier-slide-second-content carrier-slide-second-image">
                <img class="carrier-slide-second-image-content" src="assets/img/zgoda.jpg" alt="Zgoda">

                <figcaption class="carrier-slide-second-image-caption">
                    <p class="carrier-slide-second-image-caption-text"><span>"Think, believe, dream, dare"</span></p>
                    <p class="carrier-slide-second-image-caption-text"><span>(Walt Disney)</span></p>
                </figcaption>
            </figure>
            <div class="carrier-slide-second-content carrier-slide-second-description">
                <p class="carrier-slide-second-description-text"><span>My nie zmarnujemy</span></p>
                <p class="carrier-slide-second-description-text"><span>Twojego talentu*</span></p>
                <span class="carrier-slide-second-description-text-sign">*Walt Disney by nam na to nie pozwolił</span>
            </div>
        </aside>

        <section class="contact-section">
            <aside class="contact linkedin">
                <p class="contact-jobs-header">Aktualnie poszukujemy:</p>
                <ul class="contact-jobs-list">
                    <li class="contact-jobs-list-item">- Grafik</li>
                    <li class="contact-jobs-list-item">- Programista</li>
                </ul>
                <p class="follow-us">
                    Obserwuj nas:

                    <a class="linkedin-link" target="_blank" href="https://pl.linkedin.com/company/danhoss">
                        <img class="linkedin-logo" src="assets/img/linkedin.png" alt="Linkedin">
                    </a>
                </p>
            </aside>
            <form action="mailto:praca@danhoss.com" class="contact contact-form">
                <input class="contact-form-input" type="text" name="name" placeholder="imie, nazwisko, @">
                <textarea class="contact-form-textarea" name="content" placeholder="treść"></textarea>
                <div class="contact-form-group">
                    <input type="checkbox" data-accept-policy class="contact-form-checkbox" name="accept-policy" value="1"> Akceptuję politykę prywatności
                </div>
                <div class="cv">
                    <input class="contact-form-input file" type="file" name="cv">
                    <p class="cv-text">Załącz CV</p>
                </div>
                <button data-contact-submit disabled class="contact-form-submit file disabled" type="submit" name="submit">Wyślij</button>
            </form>
        </section>
    </section>
</main>
<?php require_once 'footer.php' ?>