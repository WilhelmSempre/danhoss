<?php
    $pageTitle = 'Dlaczego';
?>

<?php require_once 'head.php'; ?>
<main class="wrapper container why">
    <div class="carousel-items-wrapper">
        <section class="why-section owl-carousel owl-theme">
            <figure class="why-slide odd wow bounceInUp">
                <figcaption class="why-slide-image-caption">
                    <p class="why-slide-image-caption-text"><span>Niecodzienne chwile</span></p>
                    <p class="why-slide-image-caption-text"><span>to niecodzienne emocje</span></p>
                </figcaption>
                <img class="why-slide-image" src="assets/img/detal.jpg" alt="Detal">
            </figure>
            <figure class="why-slide even wow bounceInUp">
                <figcaption class="why-slide-image-caption">
                    <p class="why-slide-image-caption-text"><span>Śmiech zaraża</span></p>
                </figcaption>
                <img class="why-slide-image" src="assets/img/cristopher-boss.jpg" alt="Cristopher is a boss!">
            </figure>
            <figure class="why-slide odd wow bounceInUp">
                <figcaption class="why-slide-image-caption">
                    <p class="why-slide-image-caption-text"><span>Lubimy niespodzianki</span></p>
                </figcaption>
                <img class="why-slide-image" src="assets/img/suprises.jpg" alt="Suprises">
            </figure>
            <figure class="why-slide even wow bounceInUp">
                <figcaption class="why-slide-image-caption">
                    <p class="why-slide-image-caption-text"><span>Pozytywna energia wraca</span></p>
                </figcaption>
                <img class="why-slide-image" src="assets/img/energy.jpg" alt="Energy">
            </figure>
            <figure class="why-slide odd wow bounceInUp">
                <figcaption class="why-slide-image-caption">
                    <p class="why-slide-image-caption-text"><span>Pamiętamy o tych, których kochamy</span></p>
                </figcaption>
                <img class="why-slide-image" src="assets/img/love.jpg" alt="Love">
            </figure>
            <figure class="why-slide even wow bounceInUp">
                <figcaption class="why-slide-image-caption">
                    <p class="why-slide-image-caption-text"><span>Kreatywni ludzie inspirują</span></p>
                </figcaption>
                <img class="why-slide-image" src="assets/img/creative.jpg" alt="Creative">
            </figure>
        </section>
    </div>
</main>
<?php require_once 'footer.php' ?>
