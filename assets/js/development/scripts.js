'use strict';

$(document).ready(function() {

    (function() {
        let $navigation = $('.site-navigation');

        /* Hamburger */
        $('.hamburger').on('click', function(event) {
            event = event || window.event;

            $(this).toggleClass('is-active');

            if (!$navigation.is(':visible')) {
                $navigation.slideDown(200);
            } else {
                $navigation.slideUp(200);
            }

            if (!event.defaultPrevented) {
                event.preventDefault();
            }
        });

        $(window).resize(function() {
            let screenWidth = +$(this).outerWidth(true);

            if (screenWidth > 960) {
                if (!$navigation.is(':visible')) {
                    $navigation.show();
                }
            }
        });

        $('.owl-carousel').owlCarousel({
            slideSpeed : 300,
            items : 1,
            paginationSpeed : 300,
            singleItem : true,
            autoplay: true,
            loop: true,
            autoHeight:true,
            autoplayTimeout: 6000,
            dots: true
        });

        let $acceptPolicy = $('[data-accept-policy]');

        $acceptPolicy.on('change', function () {
            let isChecked = $(this).prop('checked');
            let $contactSubmit = $('[data-contact-submit]');

            if (isChecked) {
                $contactSubmit.prop('disabled', false).removeClass('disabled');
            } else {
                $contactSubmit.prop('disabled', true).addClass('disabled');
            }
        });

        let typedOptions = {
            strings: ['<span>Nasz wspólny cel, to wspólny sukces. Wierzymy we współpracę. Chcemy się rozwijać.</span>',
                '<span>Czasem się spieramy, ale zawsze gramy do jednej bramki.</span>',
                '<span>Dobry pomysł! - nie pytamy CZY go realizować, zastanawiamy się JAK to zrobić?</span>'],
            typeSpeed: 50,
            backSpeed: 0,
            backDelay: 500,
            startDelay: 1000,
            cursorChar: '',
            loop: true
        };

        if ($('.carrier-slide-second-description-text').length > 0) {
            let typed = new Typed('.carrier-slide-second-description-text', typedOptions);
        }
    })();

    new WOW().init();
});