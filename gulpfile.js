'use strict';

let gulp = require('gulp');
let merge = require('merge-stream');
let uglify = require('gulp-uglify-es').default;


let plugins = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'gulp.*', 'main-bower-files', 'merge-stream'],
    replaceString: /\bgulp[\-.]/,
    DEBUG: true
});

let bowerOptions = {overrides: {
        'css-hamburgers': {main: [
                'dist/hamburgers.css'
            ]}
    }};

let js_filter = plugins.filter('**/*.js', {'restore': true});
let css_filter = plugins.filter('**/*.css', {'restore': true});
let scss_filter = plugins.filter('**/*.scss', {'restore': true});

let bower_files = gulp.src(plugins.mainBowerFiles(bowerOptions))
    .pipe(plugins.order([
        'jquery/**/*.js',
        '**/*.js',
        '**/scripts.js'
    ], { base: './assets/vendor' }));

/**
 *
 */
let bower_js = bower_files
    .pipe(js_filter);

/**
 *
 */
let bower_scss = bower_files
        .pipe(scss_filter)
        .pipe(plugins.sass());

/**
 *
 */
let bower_css = bower_files
    .pipe(css_filter);

/**
 *
 */
let scss = gulp.src('./assets/scss/development/*.scss')
    .pipe(plugins.sass());

/**
 *
 */
gulp.task('compile_scss', ['bower_css'], function () {
    return merge(scss)
        .pipe(plugins.debug())
        .pipe(plugins.using({}))
        .pipe(plugins.concat('style.css'))
        .pipe(gulp.dest('./assets/css/development'));
});

/**
 *
 */
gulp.task('bower_css', function () {
    return merge(bower_scss, bower_css)
        .pipe(plugins.debug())
        .pipe(plugins.using({}))
        .pipe(plugins.concat('bower.css'))
        .pipe(gulp.dest('./assets/css/development'));
});

/**
 *
 */
gulp.task('bower_js', function () {
    return merge(bower_js)
        .pipe(plugins.debug())
        .pipe(plugins.using({}))
        .pipe(plugins.concat('bower.js'))
        .pipe(gulp.dest('./assets/js/development'));
});

// /**
//  *
//  */
// gulp.task('move_image', function () {
//     gulp.src('src/ExpertGroupBundle/Resources/assets/public/img/*')
//         .pipe(plugins.debug())
//         .pipe(plugins.using({}))
//         .pipe(gulp.dest('web/img'));
// });

/**
 *
 */
gulp.task('save_js', ['bower_js'], function () {
    gulp.src('./assets/js/development/*.js')
        .pipe(plugins.debug())
        .pipe(plugins.using({}))
        .pipe(uglify())
        .pipe(plugins.concat('main.min.js'))
        .pipe(gulp.dest('./assets/js/dist'));
});

/**
 *
 */
gulp.task('save_css', ['compile_scss'], function () {
    gulp.src('./assets/css/development/*.css')
        .pipe(plugins.debug())
        .pipe(plugins.using({}))
        .pipe(plugins.concat('style.min.css'))
        .pipe(plugins.cssmin())
        .pipe(gulp.dest('./assets/css/dist'));
});


//
gulp.task('default', ['save_css', 'save_js'], function() {});
